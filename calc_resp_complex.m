function respComplex=calc_resp_complex(Y,x,y,features)
iterations=size(Y,4);
respComplex(1:iterations)=zeros(1,iterations);
for i=-1:1
  for j=-1:1
	%take max across features
	resp_temp(1:iterations)=max(Y(x+i,y+j,features,:),[],3);
	%take max across space
	respComplex(1:iterations)=max(respComplex(1:iterations),resp_temp);
  end
end
