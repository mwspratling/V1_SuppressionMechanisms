----------------------------------------------------------------------------
# INTRODUCTION
----------------------------------------------------------------------------

This code implements the simulation results reported in:

[M. W. Spratling (2011) A single functional model accounts for the distinct
properties of suppression in cortical area V1, Vision Research, 51:563-76.](https://nms.kcl.ac.uk/michael.spratling/Doc/v1_suppression_mechanisms.pdf)

Please cite this paper if this code is used in, or to motivate, any publications. 

----------------------------------------------------------------------------
# USAGE
----------------------------------------------------------------------------

This code requires MATLAB and the Image Processing Toolbox. It was tested with
MATLAB Version 7.7 (R2008b) and Image Processing Toolbox Version 6.2 (R2008b).
However, it makes very little use of the Image Processing Toolbox and should be
easy to modify to work without this toolbox.

To use this software:
```
    run matlab 
    cd to the directory containing this code 
    run one of the functions with the name V1_*
```
Each of the V1_* functions runs one of the experiments reported in the above
publication. The following table gives details of which MATLAB function produces
each figure in this article.

Figure     | MATLAB Function
-----------|------------------------------------------------------------
4a,c,e,&g  | V1_cross_orient_suppression_dynamics
4b,d,f,&h  | V1_surround_suppression_dynamics
           |
5a         | V1_cross_orient_suppression_onset      
5b         | V1_surround_suppression_onset
           |
6a         | V1_cross_orient_suppression_test_contrast
6b         | V1_surround_suppression_iso_cent_contrast
6c         | V1_cross_orient_suppression_test_contrast_temporal
6d         | V1_surround_suppression_iso_cent_contrast_temporal
           |
7a&d       | V1_temporal_frequency_tuning
7b&e       | V1_cross_orient_suppression_test_contrast_temporal_freq
7c&f       | V1_surround_suppression_iso_cent_contrast_temporal_freq
           |
8a         | V1_iso_surround_with_perp_surround_contrast
8b         | V1_perp_surround_with_mask_contrast
8c         | V1_cross_orient_with_perp_surround_contrast
8d         | V1_perp_surround_with_iso_surround_contrast
