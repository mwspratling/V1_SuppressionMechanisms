function [y,Y]=dim_conv(wFFon,wFFoff,xon,xoff,iterations)
epsilon=1e-9;
psi=5000;
epsilon1=0.0001; %>0.001 this becomes significant compared to y and hence
                %produces sustained responses and more general suppression

nMasks=size(wFFon,3);
if nargin<5, iterations=50; end

%normalize weights
wFFon=single(wFFon);
wFFoff=single(wFFoff);
for i=1:nMasks
  norm=sum(sum([wFFon(:,:,i),wFFoff(:,:,i)]))./psi;
  wFFon(:,:,i)=wFFon(:,:,i)./(epsilon+norm);
  wFFoff(:,:,i)=wFFoff(:,:,i)./(epsilon+norm);
end

%initialise outputs
[a,b,z]=size(xon);
y=zeros(a,b,nMasks,'single');

%iterate to determine steady-state response
for t=1:iterations
  fprintf(1,'.%i.',t);
    
  %update outputs
  for i=1:nMasks 
	y(:,:,i)=epsilon1.*...
			 (filter2(wFFon(:,:,i),xon(:,:,min(t,z)),'same')+...
			  filter2(wFFoff(:,:,i),xoff(:,:,min(t,z)),'same'));
    y(:,:,i)=max(0,y(:,:,i));
  end
  
  %record response over time
  Y(:,:,:,t)=y;
end
disp(' ');

