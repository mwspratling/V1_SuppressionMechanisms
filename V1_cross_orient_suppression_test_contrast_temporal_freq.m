function V1_cross_orient_suppression_test_contrast_temporal_freq
linear=0;
noLGNsat=0;
crop=0;

iterations=180;
grating_wavel=6;
patch_diam=13;
contrasts=[0.0125,0.025,0.05,0.1,0.2,0.4,0.6];
drift_rates=[-1,0.05,0.1,0.2,0.5].*360;

node=1;
test_drift_rate=4;
mask_contrast=0.4;
mask_angle=90;

[wFFon,wFFoff]=dim_conv_V1_filter_definitions;

clf
dcount=0;
for mask_drift_rate=drift_rates
  dcount=dcount+1;
  tcount=0;
  for contg=contrasts
	tcount=tcount+1;
	test_phase=0;
	mask_phase=180;
	for t=1:iterations
	  if mask_drift_rate<0
		%test grating alone
		I=image_cross_orientation_sine_circular(patch_diam,patch_diam,...
												grating_wavel,grating_wavel,...
												0,mask_angle,...
												test_phase,0,...
												contg,0);
	  else	  
		%test+mask
		I=image_cross_orientation_sine_circular(patch_diam,patch_diam,...
												grating_wavel,grating_wavel,...
												0,mask_angle,...
												test_phase,mask_phase,...
												contg,mask_contrast);
	  end
	  [Ion(:,:,t),Ioff(:,:,t)]=preprocess_image(I,noLGNsat);
	  test_phase=test_phase+test_drift_rate;
	  mask_phase=mask_phase+mask_drift_rate;
	end
	[a,b]=size(I);
	%plot original image
	maxsubplot(3,length(contrasts),tcount),
	imagesc(I,[0,1]);
	axis('equal','tight'), set(gca,'XTick',[],'YTick',[],'FontSize',11);
	drawnow;
  
	%initial response without competition
	[y,ron,roff,eon,eoff,Y]=dim_conv_on_and_off(wFFon,wFFoff,Ion,Ioff,1);
	y=mean(Y,4);
	maxsubplot(3,length(contrasts),tcount+length(contrasts)),
	imagesc(y(crop+1:a-crop,crop+1:b-crop,node),[0,0.05]), 
	axis('equal','tight'), set(gca,'XTick',[],'YTick',[],'FontSize',11);
	drawnow;
	
	%perform competition 
	if linear
	  [y,Y]=linear_on_and_off(wFFon,wFFoff,Ion,Ioff,iterations);
	else
	  [y,ron,roff,eon,eoff,Y]=dim_conv_on_and_off(wFFon,wFFoff,Ion,Ioff,iterations);
	end
	y=mean(Y,4);
	maxsubplot(3,length(contrasts),tcount+2*length(contrasts)),
	imagesc(y(crop+1:a-crop,crop+1:b-crop,node),[0,0.05]), 
	axis('equal','tight'), set(gca,'XTick',[],'YTick',[],'FontSize',11);
	drawnow;

	%record neural response
	respComplex(1:iterations)=calc_resp_complex(Y,ceil(a/2),ceil(b/2),node+[0:8:31]);
	meanrespComplex(tcount,dcount)=mean(respComplex,2);
  
	respSimple(1:iterations)=Y(ceil(a/2),ceil(b/2),node,:);
	meanrespSimple(tcount,dcount)=mean(respSimple,2);

  end
end

figure(1),clf
subplot(3,2,[4,6])
semilogx(contrasts,meanrespSimple(:,1),'b-o','LineWidth',2,'MarkerSize',10,'MarkerFaceColor','b');
hold on
semilogx(contrasts,meanrespSimple(:,2),'r-o','LineWidth',4,'MarkerSize',10,'MarkerFaceColor','w');
semilogx(contrasts,meanrespSimple(:,3),'r-o','LineWidth',3,'MarkerSize',10,'MarkerFaceColor','w');
semilogx(contrasts,meanrespSimple(:,4),'r-o','LineWidth',2,'MarkerSize',10,'MarkerFaceColor','w');
semilogx(contrasts,meanrespSimple(:,5),'r-o','LineWidth',1,'MarkerSize',10,'MarkerFaceColor','w');
if linear
  axis([0.01,1,0,0.25])
  set(gca,'YTick',[0:0.1:0.25]);
else
  axis([0.01,1,0,0.005])
  set(gca,'YTick',[0:0.001:0.005]);
end
set(gca,'FontSize',20);
xlabel('Test Contrast'),ylabel('Response')


figure(2),clf
subplot(3,2,[4,6])
semilogx(contrasts,meanrespComplex(:,1),'b-o','LineWidth',2,'MarkerSize',10,'MarkerFaceColor','b');
hold on
semilogx(contrasts,meanrespComplex(:,2),'r-o','LineWidth',4,'MarkerSize',10,'MarkerFaceColor','w');
semilogx(contrasts,meanrespComplex(:,3),'r-o','LineWidth',3,'MarkerSize',10,'MarkerFaceColor','w');
semilogx(contrasts,meanrespComplex(:,4),'r-o','LineWidth',2,'MarkerSize',10,'MarkerFaceColor','w');
semilogx(contrasts,meanrespComplex(:,5),'r-o','LineWidth',1,'MarkerSize',10,'MarkerFaceColor','w');
if linear
  axis([0.01,1,0,0.5])
  set(gca,'YTick',[0:0.1:0.5]);
else
  axis([0.01,1,0,0.1])
  set(gca,'YTick',[0:0.02:0.1]);
end
set(gca,'FontSize',20);
xlabel('Test Contrast'),ylabel('Response')


figure(3),clf
maxsubplot(3,2,4)
image_diam=patch_diam+10;
spacingh=ceil((100-image_diam)/2);
L=zeros(image_diam*2+2,2*spacingh+image_diam)+0.5;
centv=ceil(image_diam/2);
for contm=[0,0.2]
  centh=ceil(image_diam/2)+1;
  for contg=contrasts([1,4,7])
	I=image_cross_orientation_sine_circular(patch_diam,5,...
											grating_wavel,grating_wavel,...
											0,mask_angle,0,0,...
											contg,contm);
	[a,b]=size(I);
	L(centv-floor(a/2):centv+floor(a/2),centh-floor(a/2):centh+floor(a/2))=I';
	centh=centh+spacingh;
  end
  centv=centv+image_diam;
end
imagesc(L,[0,1]);
axis('equal','off')
axis([-10,2*spacingh+image_diam+10,2,2*image_diam-2])
cmap=colormap('gray');
%cmap=1-cmap;
%colormap(cmap);
hold on
plot((image_diam+spacingh)/2+[0,spacingh],image_diam/2.*[1,1],'bo','LineWidth',2,'MarkerSize',10,'MarkerFaceColor','b');
plot((image_diam+spacingh)/2+[0,spacingh],3*image_diam/2.*[1,1],'ro','LineWidth',4,'MarkerSize',10,'MarkerFaceColor','w');
