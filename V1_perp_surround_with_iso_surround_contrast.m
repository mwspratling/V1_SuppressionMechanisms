function V1_perp_surround_with_iso_surround_contrast
linear=0;
noLGNsat=0;
crop=0;
grating_wavel=6;
contrasts=[0.0125,0.025,0.05,0.1,0.2,0.4,0.8,1.25];
fixed_contrast=0.2;
node=1;
patch_diam=13;
iterations=30;
drift_rate=0;

[wFFon,wFFoff]=dim_conv_V1_filter_definitions;

clf
i=0;
for iso_contrast=contrasts
  i=i+1;
  phase=0;
  for t=1:iterations
	%centre-surround pattern with two surround gratings
	I=image_contextual_surround_plaid(patch_diam,0,20,...
							    grating_wavel,grating_wavel,grating_wavel,...
								0,90,phase,phase,phase,...
								fixed_contrast,iso_contrast,fixed_contrast);
	if iso_contrast>1 %centre patch alone
	  I=image_contextual_surround(patch_diam,20,0,grating_wavel,grating_wavel,...
								  0,phase,phase,fixed_contrast,0);
	end
	[Ion(:,:,t),Ioff(:,:,t)]=preprocess_image(I,noLGNsat);
	phase=phase+drift_rate;
  end
  [a,b]=size(I);
  %plot original image
  maxsubplot(3,length(contrasts),i),
  imagesc(I(:,:,1),[0,1]);
  axis('equal','tight'), set(gca,'XTick',[],'YTick',[],'FontSize',11);
  drawnow;
  
  %initial response without competition
  [y,ron,roff,eon,eoff,Y]=dim_conv_on_and_off(wFFon,wFFoff,Ion,Ioff,1);
  y=mean(Y,4);
  maxsubplot(3,length(contrasts),i+length(contrasts)),
  imagesc(y(crop+1:a-crop,crop+1:b-crop,node),[0,0.01]), 
  axis('equal','tight'), set(gca,'XTick',[],'YTick',[],'FontSize',11);
  drawnow;
  
  %perform competition
  if linear
	[y,Y]=linear_on_and_off(wFFon,wFFoff,Ion,Ioff,1);
  else
	[y,ron,roff,eon,eoff,Y]=dim_conv_on_and_off(wFFon,wFFoff,Ion,Ioff,iterations);
  end
  y=mean(Y,4);
  maxsubplot(3,length(contrasts),i+2*length(contrasts)),
  imagesc(y(crop+1:a-crop,crop+1:b-crop,node),[0,0.01]), 
  axis('equal','tight'), set(gca,'XTick',[],'YTick',[],'FontSize',11);
  drawnow;
    
  %record neural response
  respComplex(1:iterations)=calc_resp_complex(Y,ceil(a/2),ceil(b/2),node+[0:8:31]);
  meanrespComplex(i)=mean(respComplex,2);
  
  respSimple(1:iterations)=Y(ceil(a/2),ceil(b/2),node,:);
  meanrespSimple(i)=mean(respSimple,2);
end

numc=length(contrasts)-1;

figure(1),clf
subplot(3,2,[4,6])
semilogx(contrasts(1:numc),meanrespSimple(1:numc),'r-o','LineWidth',4,'MarkerSize',12,'MarkerFaceColor','w');
axis([0.01,1,0,0.0325])
set(gca,'XTick',[0.01,0.1,1],'YTick',[0:0.01:0.06]);
set(gca,'XTickLabel',[0.01,0.1,1],'FontSize',20,'Box','on');
xlabel('Contrast'),ylabel('Response')
hold on
%plot activity to centre alone
plot([0.01,1],meanrespSimple(1+numc).*[1,1],'b-','LineWidth',4);
if linear
  axis([0.01,1,0,0.35])
  set(gca,'YTick',[0:0.1:0.5]);
end


figure(2),clf
subplot(3,2,[4,6])
semilogx(contrasts(1:numc),meanrespComplex(1:numc),'r-o','LineWidth',4,'MarkerSize',12,'MarkerFaceColor','w');
axis([0.01,1,0,0.04])
set(gca,'XTick',[0.01,0.1,1],'YTick',[0:0.01:0.06]);
set(gca,'XTickLabel',[0.01,0.1,1],'FontSize',20,'Box','on');
xlabel('Contrast'),ylabel('Response')
hold on
%plot activity to centre alone
plot([0.01,1],meanrespComplex(1+numc).*[1,1],'b-','LineWidth',4);
if linear
  axis([0.01,1,0,0.5])
  set(gca,'YTick',[0:0.1:0.35]);
end


figure(3),clf
maxsubplot(5,2,8)
maxdiam=3*patch_diam;
spacingh=maxdiam+patch_diam;
L=zeros(maxdiam+2,2*spacingh+maxdiam+1)+0.5;
centv=ceil(maxdiam/2)+1;
centh=ceil(maxdiam/2)+1;
for iso_contrast=[0,0.16,0.8]
  I=image_contextual_surround_plaid(patch_diam,0,patch_diam,grating_wavel,...
									grating_wavel,grating_wavel,0,90,0,0,0,...
									fixed_contrast,iso_contrast,fixed_contrast);
  [a,b]=size(I);
  L(centv-floor(a/2):centv+floor(a/2),centh-floor(a/2):centh+floor(a/2))=I';
  centh=centh+spacingh;
end
imagesc(L,[0,1]);
axis('equal','off')
ends=10;
axis([-ends,2*spacingh+maxdiam+ends,1,maxdiam]);
cmap=colormap('gray');
%cmap=1-cmap;
%colormap(cmap);
