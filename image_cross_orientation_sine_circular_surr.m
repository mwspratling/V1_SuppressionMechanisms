function I=image_cross_orientation_sine_circular_surr(csize,vsize,ssize,wavelc,wavelm,wavels,angle,diffm,diffs,phasec,phasem,phases,contc,contm,conts)
% csize  = the diameter of the centre patch (pixels)
% vsize  = the width of the blank area between the centre and surround
% ssize  = the width of the surrounding annulus  (pixels)
% wavelc  = the wavelength of the principal grating (pixels)
% wavelm  = the wavelength of the mask grating (pixels)
% wavels = the wavelength of the sin wave in the surround (pixels)
% angle = angle of the principal grating
% diffm = angle between the principal grating and the mask
% diffs = angle between the principal grating and surround sinusoid
% phasec = the phase of the principal grating
% phasem = the phase of the mask grating
% phases = the phase of the surround grating
% contc = contrast of the principal grating
% contm = contrast of the mask grating
% contm = contrast of the surround grating

freqc=2*pi./wavelc;
freqm=2*pi./wavelm;
freqs=2*pi./wavels;
angle=-angle*pi/180;
diffm=-diffm*pi/180;
diffs=-diffs*pi/180;
phasec=phasec*pi/180;
phasem=phasem*pi/180;
phases=phases*pi/180;

%define image size
sz=fix(csize+2*vsize+2*ssize);
if mod(sz,2)==0, sz=sz+1;end %image has odd dimension
I=zeros(sz);

%define mesh on which to draw sinusoids
[x y]=meshgrid(-fix(sz/2):fix(sz/2),fix(-sz/2):fix(sz/2));
yc=-x*sin(angle)+y*cos(angle);
ym=-x*sin(angle+diffm)+y*cos(angle+diffm);
ys=-x*sin(angle+diffs)+y*cos(angle+diffs);

%make sinusoids with values ranging from 0 to 1 (i.e. contrast is positive)
grating=contc.*cos(freqc*yc+phasec);
mask=contm.*cos(freqm*ym+phasem);
surround=0.5+0.5*conts.*cos(freqs*ys+phases);
plaid=0.5+0.5.*(grating+mask);

%define radius from centre point
radius=sqrt(x.^2+y.^2);

%put togeter image from components
I=surround;
I(find(radius<vsize+csize/2))=0.5;
I(find(radius<csize/2))=plaid(find(radius<csize/2));
I(find(radius>ssize+vsize+csize/2))=0.5;
