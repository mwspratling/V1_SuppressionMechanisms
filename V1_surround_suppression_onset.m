function V1_surround_suppression_onset
linear=0;
noLGNsat=0;
complex=0;
crop=14;
grating_wavel=6;
patch_diam=11;
gap=2;
image_size=3*patch_diam+2*gap;

iterations=26-5;
switchTime=11-5;
preshow=5;
contrast=1;
phase=0;
if complex
  drift_rate=5
else
  drift_rate=0;
end
node=1;
[wFFon,wFFoff]=dim_conv_V1_filter_definitions;

figure(2),clf
for test=1:2
  phase=0;
  for t=1:switchTime
	%large grating
	I=image_centre_surround(patch_diam,gap,0.5*(image_size-patch_diam),...
							grating_wavel,grating_wavel,90,90,phase,phase,...
							contrast,contrast); 
	if t==1 & linear==0 & noLGNsat==0
	  axes('Position',[0.15,0.225,0.2,0.2]),imagesc(I',[0,1]);
	  axis('equal','tight','off')
	end
	[Ion(:,:,t),Ioff(:,:,t)]=preprocess_image(I,noLGNsat);
	phase=phase+drift_rate;
  end
  for t=switchTime+1:iterations
	if test==1,	%large grating
	  I=image_centre_surround(patch_diam,gap,0.5*(image_size-patch_diam),...
							  grating_wavel,grating_wavel,0,0,phase,phase,...
							  contrast,contrast); 
	  
	  if t==iterations & linear==0 & noLGNsat==0
		axes('Position',[0.65,0.39,0.2,0.2]),imagesc(I',[0,1]);
		axis('equal','tight','off')
	  end
	else 	%contextual surround
	  I=image_centre_surround(patch_diam,gap,0.5*(image_size-patch_diam),...
							  grating_wavel,grating_wavel,0,90,phase,phase,...
							  contrast,contrast); 
	  if t==iterations & linear==0 & noLGNsat==0
		axes('Position',[0.65,0.6,0.2,0.2]),imagesc(I',[0,1]);
		axis('equal','tight','off')
	  end
	end
	drawnow
	[Ion(:,:,t),Ioff(:,:,t)]=preprocess_image(I,noLGNsat);
	phase=phase+drift_rate;
  end
  [a,b]=size(I);
  
  %perform competition
  if linear
	[y,Y]=linear_on_and_off(wFFon,wFFoff,Ion,Ioff,iterations);
  else
	[y,ron,roff,eon,eoff,Y]=dim_conv_on_and_off(wFFon,wFFoff,Ion,Ioff,iterations);
  end

  %record neural response
  respComplex(test,1:iterations)=calc_resp_complex(Y,ceil(a/2),ceil(b/2),node+[0:8:31]);
  
  respSimple(test,1:iterations)=Y(ceil(a/2),ceil(b/2),node,:);
  
end
if complex
  resp=respComplex;
else
  resp=respSimple;
end

axes('Position',[0.3,0.3,0.4,0.4],'FontSize',18)
plot(resp(1,:)','r-','LineWidth',5)
hold on
plot(resp(2,:)','b-','LineWidth',3)
plot(resp(2,:)'-resp(1,:)','k--','LineWidth',2)
plot([switchTime,switchTime],[-1,1],'k:')
tickpoints=[switchTime-preshow,switchTime:preshow:iterations];
if linear==0 & noLGNsat==0
  set(gca,'color', 'none','XTick',tickpoints,'XTickLabel',tickpoints-switchTime);
  ylabel('Response');
  xlabel('Time (iterations)');
else
  set(gca,'XTickLabel',[],'YTickLabel',[])
end
axis([switchTime-preshow,iterations,max(max(resp))*-0.05,max(max(resp))*1.05])
cmap=colormap('gray');

diff=resp(1,:)-resp(2,:);
if diff(iterations)<0, diff=-diff; end
ref=max(diff)*0.05;
sig=find(diff>=ref);
sig=sig(find(sig>=switchTime));
sig(1)-switchTime-1+(ref-diff(sig(1)-1))./(diff(sig(1))-diff(sig(1)-1))

