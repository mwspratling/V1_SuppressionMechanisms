function [y,ron,roff,eon,eoff,Y,Eon,Eoff]=dim_conv(wFFon,wFFoff,xon,xoff,iterations)
epsilon=1e-9;
psi=5000;
epsilon1=0.0001; %>0.001 this becomes significant compared to y and hence
                %produces sustained responses and more general suppression
epsilon2=500*epsilon1*psi;%this determines scaling of initial transient response
						  %(i.e. response to linear filters).

nMasks=size(wFFon,3);
if nargin<5, iterations=50; end

%normalize weights
wFFon=single(wFFon);
wFFoff=single(wFFoff);
for i=1:nMasks
  norm=sum(sum([wFFon(:,:,i),wFFoff(:,:,i)]))./psi;
  wFFon(:,:,i)=wFFon(:,:,i)./(epsilon+norm);
  wFFoff(:,:,i)=wFFoff(:,:,i)./(epsilon+norm);
  norm=max(max([wFFon(:,:,i),wFFoff(:,:,i)]))./psi;
  wFBon(:,:,i)=wFFon(:,:,i)./(epsilon+norm);
  wFBoff(:,:,i)=wFFoff(:,:,i)./(epsilon+norm);
end

%initialise outputs
[a,b,z]=size(xon);
y=zeros(a,b,nMasks,'single');
%iterate to determine steady-state response
for t=1:iterations
  fprintf(1,'.%i.',t);
    
  %update error units
  ron=zeros(a,b,'single');
  roff=zeros(a,b,'single');
  for i=1:nMasks 
	ron=ron+conv2(y(:,:,i),wFBon(:,:,i),'same');
	roff=roff+conv2(y(:,:,i),wFBoff(:,:,i),'same');
  end
  eon=xon(:,:,min(t,z))./(epsilon2+ron);
  eoff=xoff(:,:,min(t,z))./(epsilon2+roff);

  %update outputs
  for i=1:nMasks 
	y(:,:,i)=(epsilon1+y(:,:,i)).*...
		 (filter2(wFFon(:,:,i),eon,'same')+filter2(wFFoff(:,:,i),eoff,'same')); 
	%y(:,:,i)=(filter2(wFFon(:,:,i),xon(:,:,min(t,z)),'same')+filter2(wFFoff(:,:,i),xoff(:,:,min(t,z)),'same'));
    y(:,:,i)=max(0,y(:,:,i));
  end
  
  %record response over time
  if nargout>=6, Y(:,:,:,t)=y; end
  if nargout>=7, Eon(:,:,t)=eon; end
  if nargout>=8, Eoff(:,:,t)=eoff; end
end
disp(' ');

