function V1_cross_orient_suppression_test_contrast
linear=0;
noLGNsat=0;
crop=0;

iterations=10;
grating_wavel=6;
patch_diam=13;
contrasts=[0.0125,0.025,0.05,0.1,0.2,0.4,0.6];
node=1;

mask_angle=90;

[wFFon,wFFoff]=dim_conv_V1_filter_definitions;

clf
pcount=0;
for phase=[0,180];
  pcount=pcount+1;
  tcount=0;
  for contg=contrasts
	tcount=tcount+1;
	mcount=0;
	for contm=[0,0.4]
	  mcount=mcount+1;
	  for t=1:iterations
		I=image_cross_orientation_sine_circular(patch_diam,patch_diam,...
												grating_wavel,grating_wavel,...
												0,mask_angle,phase,phase,...
												contg,contm);
		[Ion(:,:,t),Ioff(:,:,t)]=preprocess_image(I,noLGNsat);
	  end
	  [a,b,z]=size(I);
	  %plot original image
	  maxsubplot(3,length(contrasts),tcount),
	  imagesc(I,[0,1]);
	  axis('equal','tight'), set(gca,'XTick',[],'YTick',[],'FontSize',11);
	  drawnow;
	  
	  %initial response without competition
	  [y,ron,roff,eon,eoff,Y]=dim_conv_on_and_off(wFFon,wFFoff,Ion,Ioff,1);
	  y=mean(Y,4);
	  maxsubplot(3,length(contrasts),tcount+length(contrasts)),
	  imagesc(y(crop+1:a-crop,crop+1:b-crop,node),[0,0.05]), 
	  axis('equal','tight'), set(gca,'XTick',[],'YTick',[],'FontSize',11);
	  drawnow;
	  
	  %perform competition 
	  if linear
		[y,Y]=linear_on_and_off(wFFon,wFFoff,Ion,Ioff,iterations);
	  else
		[y,ron,roff,eon,eoff,Y]=dim_conv_on_and_off(wFFon,wFFoff,Ion,Ioff,iterations);
	  end
	  y=mean(Y,4);
	  maxsubplot(3,length(contrasts),tcount+2*length(contrasts)),
	  imagesc(y(crop+1:a-crop,crop+1:b-crop,node),[0,0.05]), 
	  axis('equal','tight'), set(gca,'XTick',[],'YTick',[],'FontSize',11);
	  drawnow;
	  
	  %record neural response
	  respComplex(1:iterations)=calc_resp_complex(Y,ceil(a/2),ceil(b/2),node+[0:8:31]);
	  meanrespComplex(tcount,mcount,pcount)=mean(respComplex,2);
  
	  respSimple(1:iterations)=Y(ceil(a/2),ceil(b/2),node,:);
	  meanrespSimple(tcount,mcount,pcount)=mean(respSimple,2);

	end
  end
end

figure(1),clf
subplot(3,2,[4,6])
semilogx(contrasts,meanrespSimple(:,1,1),'b-o','LineWidth',2,'MarkerSize',10,'MarkerFaceColor','b');
hold on
semilogx(contrasts,meanrespSimple(:,2,1),'r-o','LineWidth',4,'MarkerSize',10,'MarkerFaceColor','w');
semilogx(contrasts,meanrespSimple(:,1,2),'b--','LineWidth',2);
semilogx(contrasts,meanrespSimple(:,2,2),'r--','LineWidth',4);
if linear
  axis([0.01,1,0,0.5])
  set(gca,'YTick',[0:0.1:0.5]);
else
  axis([0.01,1,0,0.02])
  set(gca,'YTick',[0:0.01:0.02]);
end
set(gca,'FontSize',20);
xlabel('Test Contrast'),ylabel('Response')



figure(2),clf
subplot(3,2,[4,6])
semilogx(contrasts,meanrespComplex(:,1,1),'b-o','LineWidth',2,'MarkerSize',10,'MarkerFaceColor','b');
hold on
semilogx(contrasts,meanrespComplex(:,2,1),'r-o','LineWidth',4,'MarkerSize',10,'MarkerFaceColor','w');
semilogx(contrasts,meanrespComplex(:,1,2),'b--','LineWidth',2);
semilogx(contrasts,meanrespComplex(:,2,2),'r--','LineWidth',4);
if linear
  axis([0.01,1,0,0.5])
  set(gca,'YTick',[0:0.1:0.5]);
else
  axis([0.01,1,0,0.02])
  set(gca,'YTick',[0:0.01:0.02]);
end
set(gca,'FontSize',20);
xlabel('Test Contrast'),ylabel('Response')


figure(3),clf
maxsubplot(3,2,4)
image_diam=patch_diam+10;
spacingh=ceil((100-image_diam)/2);
L=zeros(image_diam*2+2,2*spacingh+image_diam)+0.5;
centv=ceil(image_diam/2);
for contm=[0,0.2]
  centh=ceil(image_diam/2)+1;
  for contg=contrasts([1,4,7])
	I=image_cross_orientation_sine_circular(patch_diam,5,...
											grating_wavel,grating_wavel,...
											0,mask_angle,phase,phase,...
											contg,contm);
	[a,b]=size(I);
	L(centv-floor(a/2):centv+floor(a/2),centh-floor(a/2):centh+floor(a/2))=I';
	centh=centh+spacingh;
  end
  centv=centv+image_diam;
end
imagesc(L,[0,1]);
axis('equal','off')
axis([-10,2*spacingh+image_diam+10,2,2*image_diam-2])
cmap=colormap('gray');
%cmap=1-cmap;
%colormap(cmap);
hold on
plot((image_diam+spacingh)/2+[0,spacingh],image_diam/2.*[1,1],'bo','LineWidth',2,'MarkerSize',10,'MarkerFaceColor','b');
plot((image_diam+spacingh)/2+[0,spacingh],3*image_diam/2.*[1,1],'ro','LineWidth',4,'MarkerSize',10,'MarkerFaceColor','w');
