function V1_surround_suppression_iso_cent_contrast_temporal
linear=0;
noLGNsat=0;
crop=0;
iterations=180;
grating_wavel=6;
patch_diam=13;
contrasts=[0.0125,0.025,0.05,0.1,0.2,0.4,1];
node=1;
surr_drift_rate=4;
cent_drift_rate=4;

[wFFon,wFFoff]=dim_conv_V1_filter_definitions;

clf
scount=0;
for conts=[0,0.4];
  scount=scount+1;
  ccount=0;
  clear Ion Ioff;
  for contc=contrasts
	ccount=ccount+1;
	cent_phase=0;
	surr_phase=0;
	for t=1:iterations
	  I=image_contextual_surround(patch_diam,0,patch_diam,grating_wavel,grating_wavel,0,cent_phase,surr_phase,contc,conts);
	  [Ion(:,:,t),Ioff(:,:,t)]=preprocess_image(I,noLGNsat);
	  cent_phase=cent_phase+cent_drift_rate;
	  surr_phase=surr_phase+surr_drift_rate;
	end
	[a,b,z]=size(Ion);
	
	%plot original image
	maxsubplot(3,length(contrasts),ccount),
	imagesc(I(crop+1:a-crop,crop+1:b-crop,:),[0,0.5]); 
	axis('equal','tight'), set(gca,'XTick',[],'YTick',[],'FontSize',11);
	drawnow;
	
	%initial response without competition
	[y,ron,roff,eon,eoff,Y]=dim_conv_on_and_off(wFFon,wFFoff,Ion,Ioff,1);
	y=mean(Y,4);
	maxsubplot(3,length(contrasts),ccount+length(contrasts)),
	imagesc(y(crop+1:a-crop,crop+1:b-crop,node),[0,0.05]), 
	axis('equal','tight'), set(gca,'XTick',[],'YTick',[],'FontSize',11);
	drawnow;
	
	%perform competition
	if linear
	  [y,Y]=linear_on_and_off(wFFon,wFFoff,Ion,Ioff,iterations);
	else
	  [y,ron,roff,eon,eoff,Y]=dim_conv_on_and_off(wFFon,wFFoff,Ion,Ioff,iterations);
	end
	y=mean(Y,4);
	maxsubplot(3,length(contrasts),ccount+2*length(contrasts)),
	imagesc(y(crop+1:a-crop,crop+1:b-crop,node),[0,0.01]), 
	axis('equal','tight'), set(gca,'XTick',[],'YTick',[],'FontSize',11);
	drawnow;
	
	%record neural response
	respComplex(1:iterations)=calc_resp_complex(Y,ceil(a/2),ceil(b/2),node+[0:8:31]);
	meanrespComplex(scount,ccount)=mean(respComplex,2);
  
	respSimple(1:iterations)=Y(ceil(a/2),ceil(b/2),node,:);
	meanrespSimple(scount,ccount)=mean(respSimple,2);

  end
end

figure(1),clf
subplot(3,2,[4,6])
semilogx(contrasts,meanrespSimple(1,:),'b-o','LineWidth',2,'MarkerSize',10,'MarkerFaceColor','b');
hold on
semilogx(contrasts,meanrespSimple(2,:),'r-o','LineWidth',4,'MarkerSize',10,'MarkerFaceColor','w');
if linear
  axis([0.01,1,0,0.25])
  set(gca,'YTick',[0:0.1:0.25]);
else
  axis([0.01,1,0,0.005])
  set(gca,'YTick',[0:0.001:0.005]);
end
set(gca,'FontSize',20);
xlabel('Centre Contrast'),ylabel('Response')


figure(2),clf
subplot(3,2,[4,6])
semilogx(contrasts,meanrespComplex(1,:),'b-o','LineWidth',2,'MarkerSize',10,'MarkerFaceColor','b');
hold on
semilogx(contrasts,meanrespComplex(2,:),'r-o','LineWidth',4,'MarkerSize',10,'MarkerFaceColor','w');
if linear
  axis([0.01,1,0,0.25])
  set(gca,'YTick',[0:0.1:0.25]);
else
  axis([0.01,1,0,0.1])
  set(gca,'YTick',[0:0.02:0.1]);
end
set(gca,'FontSize',20);
xlabel('Centre Contrast'),ylabel('Response')


figure(3),clf
maxsubplot(3,2,4)
image_diam=patch_diam*2;
spacingh=(125-image_diam)/2;
L=zeros(image_diam*2+2,2*spacingh+image_diam)+0.5;
centv=ceil(image_diam/2)+1;
for conts=[0,0.4];
  centh=ceil(image_diam/2)+2;
  for contc=contrasts([1,4,7])
	I=image_contextual_surround(patch_diam,0,patch_diam/2,grating_wavel,grating_wavel,0,0,0,contc,conts);
	[a,b]=size(I);
	L(centv-floor(a/2):centv+floor(a/2),centh-floor(a/2):centh+floor(a/2))=I';
	centh=centh+spacingh;
  end
   centv=centv+image_diam;
end
imagesc(L,[0,1]);
axis('equal','off')
ends=10;
axis([-10,2*spacingh+image_diam+10,0,2*image_diam])
cmap=colormap('gray');
%cmap=1-cmap;
%colormap(cmap);
hold on
plot(1+(image_diam+spacingh)/2+[0,spacingh],image_diam/2.*[1,1],'bo','LineWidth',2,'MarkerSize',10,'MarkerFaceColor','b');
plot(1+(image_diam+spacingh)/2+[0,spacingh],3*image_diam/2.*[1,1],'ro','LineWidth',4,'MarkerSize',10,'MarkerFaceColor','w');
