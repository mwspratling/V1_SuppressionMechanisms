function V1_temporal_frequency_tuning
crop=0;
iterations=180;
grating_wavel=6;
patch_diam=13;
drift_rates=[3,5,10,[0.05,0.1,0.2,0.5].*360];

angle=0;
node=1;

[wFFon,wFFoff]=dim_conv_V1_filter_definitions;

clf
dcount=0;
for drift_rate=drift_rates
  dcount=dcount+1;
  fprintf(1,'.%i.',dcount); 
  phase=0;
  for t=1:iterations
	I=image_circular_grating(patch_diam,patch_diam,grating_wavel,angle,phase,0.5);
	[Ion(:,:,t),Ioff(:,:,t)]=preprocess_image(I);
	phase=phase+drift_rate;
  end
  [a,b]=size(I);
  %plot original image
  maxsubplot(3,length(drift_rates),dcount),
  imagesc(I,[0,1]);
  axis('equal','tight'), set(gca,'XTick',[],'YTick',[],'FontSize',11);
  drawnow;
  
  %initial response without competition
  [y,ron,roff,eon,eoff,Y]=dim_conv_on_and_off(wFFon,wFFoff,Ion,Ioff,1);
  y=mean(Y,4);
  maxsubplot(3,length(drift_rates),dcount+length(drift_rates)),
  imagesc(y(crop+1:a-crop,crop+1:b-crop,node),[0,0.05]), 
  axis('equal','tight'), set(gca,'XTick',[],'YTick',[],'FontSize',11);
  drawnow;
  
  %perform competition 
  [y,ron,roff,eon,eoff,Y]=dim_conv_on_and_off(wFFon,wFFoff,Ion,Ioff,iterations);
  y=mean(Y,4);
  maxsubplot(3,length(drift_rates),dcount+2*length(drift_rates)),
  imagesc(y(crop+1:a-crop,crop+1:b-crop,node),[0,0.025]), 
  axis('equal','tight'), set(gca,'XTick',[],'YTick',[],'FontSize',11);
  drawnow;

  %record neural response
  respComplex(1:iterations)=calc_resp_complex(Y,ceil(a/2),ceil(b/2),node+[0:8:31]);
  meanrespComplex(dcount)=mean(respComplex,2);
  
  respSimple(1:iterations)=Y(ceil(a/2),ceil(b/2),node,:);
  meanrespSimple(dcount)=mean(respSimple,2);

  respSimpleSummed(dcount)=sum(sum(sum(y(ceil(a/2)-5:5+ceil(a/2),ceil(b/2)-5:5+ceil(b/2),:))));
end

drift_rates=drift_rates./360; %convert to cycles / iteration

figure(1),clf
subplot(3,2,[4,6])
semilogx(drift_rates,meanrespSimple,'r-o','LineWidth',4,'MarkerSize',12,'MarkerFaceColor','w');
axis([0.005,0.5,0,0.0055]);
set(gca,'XTick',[0.005,0.05,0.5],'YTick',[0:0.001:0.005],'FontSize',20,'Box','on');
set(gca,'XTickLabel',[0.005,0.05,0.5]);
xlabel('Drift Rate (cycles/iteration)'),ylabel('Response')


figure(2),clf
subplot(3,2,[4,6])
semilogx(drift_rates,meanrespComplex,'r-o','LineWidth',4,'MarkerSize',12,'MarkerFaceColor','w');
axis([0.005,0.5,0,0.1]);
set(gca,'XTick',[0.005,0.05,0.5],'YTick',[0:0.02:0.1],'FontSize',20,'Box','on');
set(gca,'XTickLabel',[0.005,0.05,0.5]);
xlabel('Drift Rate (cycles/iteration)'),ylabel('Response')


figure(3),clf
subplot(3,2,[4,6])
semilogx(drift_rates,respSimpleSummed,'r-o','LineWidth',4,'MarkerSize',12,'MarkerFaceColor','w');
axis([0.005,0.5,0,4]);
set(gca,'XTick',[0.005,0.05,0.5],'YTick',[0:1:5],'FontSize',20,'Box','on');
set(gca,'XTickLabel',[0.005,0.05,0.5]);
xlabel('Drift Rate (cycles/iteration)'),ylabel('Response')


figure(4),clf
maxsubplot(4,2,4)
spacingh=ceil((60-patch_diam)/2);
L=zeros(patch_diam+1,2*spacingh+patch_diam+1)+0.5;
centv=ceil(patch_diam/2)+1;
centh=ceil(patch_diam/2)+1;
for drift_rate=1:3
  phase=0+drift_rate;
  I=image_circular_grating(patch_diam,0,grating_wavel,angle,phase,0.5);
  [a,b]=size(I);
  L(centv-floor(a/2):centv+floor(a/2),centh-floor(a/2):centh+floor(a/2))=I';
  centh=centh+spacingh;
end
imagesc(L);
axis('equal','off')
ends=15;
axis([-ends,2*spacingh+patch_diam+ends,0,patch_diam+1])

cmap=colormap('gray');
%cmap=1-cmap;
%colormap(cmap);
