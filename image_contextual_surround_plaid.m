function I=image_contextual_surround_plaid(csize,vsize,ssize,cwavel,s1wavel,s2wavel,angle1,angle2,phasec,phases1,phases2,contc,cont1s,cont2s)
% csize  = the diameter of the centre patch (pixels)
% vsize  = the width of the blank area between the centre and surround
% ssize  = the width of the surrounding annulus  (pixels)
% cwavel  = the wavelength of the sin wave in the centre patch (pixels)
% s1wavel  = the wavelength of the 1st sin wave in the surround (pixels)
% s2wavel  = the wavelength of the 2nd sin wave in the surround (pixels)
% angle1 = angle between centre sinusoid and 1st surround sinusoid
% angle2 = angle between centre sinusoid and 2nd surround sinusoid
% phasec = the phase of the central grating
% phases1 = the phase of the 1st sin wave in the surround
% phases2 = the phase of the 2nd sin wave in the surround
% contc = contrast of central patch
% cont1s = contrast of 1st surround sinusoid
% cont2s = contrast of 2st surround sinusoid

cfreq=2*pi./cwavel;
s1freq=2*pi./s1wavel;
s2freq=2*pi./s2wavel;
angle1=-angle1*pi/180;
angle2=-angle2*pi/180;
phasec=phasec*pi/180;
phases1=phases1*pi/180;
phases2=phases2*pi/180;

%define image size
sz=fix(csize+2*vsize+2*ssize);
if mod(sz,2)==0, sz=sz+1;end %image has odd dimension
I=zeros(sz);

%define mesh on which to draw sinusoids
[x y]=meshgrid(-fix(sz/2):fix(sz/2),fix(-sz/2):fix(sz/2));
yc=-x*sin(0)+y*cos(0);
ys1=-x*sin(angle1)+y*cos(angle1);
ys2=-x*sin(angle2)+y*cos(angle2);

%make sinusoids with values ranging from 0 to 1 (i.e. contrast is positive)
center=0.5+0.5.*contc.*cos(cfreq*yc+phasec);
surround=0.5+0.5.*cont1s.*cos(s1freq*ys1+phases1)+0.5.*cont2s.*cos(s2freq*ys2+phases2);

%define radius from centre point
radius=sqrt(x.^2+y.^2); %for circular gratings
%radius=max(abs(x),abs(y)); %for square gratings

%put togeter image from components
I=surround;
I(find(radius<vsize+csize/2))=0.5;
I(find(radius<csize/2))=center(find(radius<csize/2));
I(find(radius>ssize+vsize+csize/2))=0.5;

