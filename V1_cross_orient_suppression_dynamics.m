function V1_cross_orient_suppression_dynamics
linear=0;
noLGNsat=1;
complex=0;
crop=14;
grating_wavel=6;
patch_diam=11;
image_size=3*patch_diam;
if linear
  respmax=0.5;
elseif noLGNsat
  respmax=0.25;
else
  respmax=0.11;
end
iterations=41;
switchTime=26;
preshow=5;
contrast=0.5;
phase=0;
if complex
  drift_rate=5
else
  drift_rate=0;
end
node=1;
[wFFon,wFFoff]=dim_conv_V1_filter_definitions;

figure(1),clf
for test=1:2
  phase=0;
  for t=1:switchTime
	%blank
	I=zeros(image_size,image_size)+0.5;
	if t==1 & linear==0 & noLGNsat==0 
	  axes('Position',[0.15,0.225,0.2,0.2]),imagesc(I',[0,1]);
	  axis('equal','tight','off')
	end
	[Ion(:,:,t),Ioff(:,:,t)]=preprocess_image(I,noLGNsat);
  end
  for t=switchTime+1:iterations
	if test==1,
	  %blank
	  I=zeros(image_size,image_size)+0.5;
	  if t==iterations & linear==0 & noLGNsat==0
		axes('Position',[0.65,0.225,0.2,0.2]),imagesc(I',[0,1]);
		axis('equal','tight','off')
	  end
	else
	  I=image_circular_grating(patch_diam,0.5*(image_size-patch_diam),...
							   grating_wavel,0,phase,contrast);
	  if t==iterations & linear==0 & noLGNsat==0
		axes('Position',[0.65,0.44,0.2,0.2]),imagesc(I',[0,1]);
		axis('equal','tight','off')
	  end
	end
	drawnow;

	[Ion(:,:,t),Ioff(:,:,t)]=preprocess_image(I,noLGNsat);
	phase=phase+drift_rate;
  end
  [a,b]=size(I);
  
  %perform competition 
  if linear
	[y,Y]=linear_on_and_off(wFFon,wFFoff,Ion,Ioff,iterations);
  else
	[y,ron,roff,eon,eoff,Y]=dim_conv_on_and_off(wFFon,wFFoff,Ion,Ioff,iterations);
  end

  %record neural response
  respComplex(test,1:iterations)=calc_resp_complex(Y,ceil(a/2),ceil(b/2),node+[0:8:31]);
  
  respSimple(test,1:iterations)=Y(ceil(a/2),ceil(b/2),node,:);

end
if complex
  resp=respComplex;
else
  resp=respSimple;
end

axes('Position',[0.3,0.3,0.4,0.4],'FontSize',18)
plot(resp(1,:)','b-','LineWidth',3)
hold on
plot(resp(2,:)','r-','LineWidth',5)
plot([switchTime,switchTime],[-respmax,respmax*2],'k:')
tickpoints=[switchTime-preshow,switchTime,switchTime+preshow,switchTime+2*preshow,iterations];
if linear==0 & noLGNsat==0
  set(gca,'color', 'none','XTick',tickpoints,'XTickLabel',tickpoints-switchTime);
  ylabel('Response');
  xlabel('Time (iterations)');
else
  set(gca,'XTickLabel',[],'YTickLabel',[])
end
axis([switchTime-preshow,iterations,respmax*-0.05,respmax*1.05])
cmap=colormap('gray');

diff=resp(1,:)-resp(2,:);
if diff(iterations)<0, diff=-diff; end
ref=max(diff)*0.05;
sig=find(diff>=ref);
sig=sig(find(sig>=switchTime));
sig(1)-switchTime-1+(ref-diff(sig(1)-1))./(diff(sig(1))-diff(sig(1)-1))



figure(2),clf
for test=1:2
  phase=0;
  for t=1:switchTime
	I=image_circular_grating(patch_diam,0.5*(image_size-patch_diam),...
							 grating_wavel,0,phase,contrast);
	if t==1 & linear==0 & noLGNsat==0 
	  axes('Position',[0.15,0.49,0.2,0.2]),imagesc(I',[0,1]);
	  axis('equal','tight','off')
	end
	[Ion(:,:,t),Ioff(:,:,t)]=preprocess_image(I,noLGNsat);
	phase=phase+drift_rate;
  end
  for t=switchTime+1:iterations
	if test==1,
	  I=image_circular_grating(patch_diam,0.5*(image_size-patch_diam),...
							   grating_wavel,0,phase,contrast);
	  if t==iterations & linear==0 & noLGNsat==0
		axes('Position',[0.65,0.55,0.2,0.2]),imagesc(I',[0,1]);
		axis('equal','tight','off')
	  end
	else
	  I=zeros(image_size,image_size)+0.5;
	  if t==iterations & linear==0 & noLGNsat==0
		axes('Position',[0.65,0.225,0.2,0.2]),imagesc(I',[0,1]);
		axis('equal','tight','off')
	  end
	end
	drawnow;

	[Ion(:,:,t),Ioff(:,:,t)]=preprocess_image(I,noLGNsat);
	phase=phase+drift_rate;
  end
  [a,b]=size(I);
  
  %perform competition 
  if linear
	[y,Y]=linear_on_and_off(wFFon,wFFoff,Ion,Ioff,iterations);
  else
	[y,ron,roff,eon,eoff,Y]=dim_conv_on_and_off(wFFon,wFFoff,Ion,Ioff,iterations);
  end

  %record neural response
  respComplex(test,1:iterations)=calc_resp_complex(Y,ceil(a/2),ceil(b/2),node+[0:8:31]);
  
  respSimple(test,1:iterations)=Y(ceil(a/2),ceil(b/2),node,:);

end
if complex
  resp=respComplex;
else
  resp=respSimple;
end

axes('Position',[0.3,0.3,0.4,0.4],'FontSize',18)
plot(resp(1,:)','b-','LineWidth',3)
hold on
plot(resp(2,:)','r-','LineWidth',5)
plot([switchTime,switchTime],[-respmax,respmax*2],'k:')
tickpoints=[switchTime-preshow,switchTime,switchTime+preshow,switchTime+2*preshow,iterations];
if linear==0 & noLGNsat==0
  set(gca,'color', 'none','XTick',tickpoints,'XTickLabel',tickpoints-switchTime);
  ylabel('Response');
  xlabel('Time (iterations)');
else
  set(gca,'XTickLabel',[],'YTickLabel',[])
end
axis([switchTime-preshow,iterations,respmax*-0.05,respmax*1.05])
cmap=colormap('gray');

diff=resp(1,:)-resp(2,:);
if diff(iterations)<0, diff=-diff; end
ref=max(diff)*0.05;
sig=find(diff>=ref);
sig=sig(find(sig>=switchTime));
sig(1)-switchTime-1+(ref-diff(sig(1)-1))./(diff(sig(1))-diff(sig(1)-1))


figure(3),clf
for test=1:2
  phase=0;
  for t=1:switchTime
	I=image_circular_grating(patch_diam,0.5*(image_size-patch_diam),...
							   grating_wavel,0,phase,contrast);
	if t==1 & linear==0 & noLGNsat==0 
	  axes('Position',[0.15,0.49,0.2,0.2]),imagesc(I',[0,1]);
	  axis('equal','tight','off')
	end
	[Ion(:,:,t),Ioff(:,:,t)]=preprocess_image(I,noLGNsat);
	phase=phase+drift_rate;
  end
  for t=switchTime+1:iterations
	if test==1,
	  I=image_circular_grating(patch_diam,0.5*(image_size-patch_diam),...
							   grating_wavel,0,phase,contrast);
	  if t==iterations & linear==0 & noLGNsat==0
		axes('Position',[0.65,0.55,0.2,0.2]),imagesc(I',[0,1]);
		axis('equal','tight','off')
	  end
	else
	  I=image_cross_orientation_sine_circular(patch_diam,...
											  0.5*(image_size-patch_diam),...
											  grating_wavel,grating_wavel,...
											  0,90,phase,phase,contrast, ...
											  contrast);
	  if t==iterations & linear==0 & noLGNsat==0
		axes('Position',[0.65,0.36,0.2,0.2]),imagesc(I',[0,1]);
		axis('equal','tight','off')
	  end
	end
	drawnow;

	[Ion(:,:,t),Ioff(:,:,t)]=preprocess_image(I,noLGNsat);
	phase=phase+drift_rate;
  end
  [a,b]=size(I);
  
  %perform competition 
  if linear
	[y,Y]=linear_on_and_off(wFFon,wFFoff,Ion,Ioff,iterations);
  else
	[y,ron,roff,eon,eoff,Y]=dim_conv_on_and_off(wFFon,wFFoff,Ion,Ioff,iterations);
  end

  %record neural response
  respComplex(test,1:iterations)=calc_resp_complex(Y,ceil(a/2),ceil(b/2),node+[0:8:31]);
  
  respSimple(test,1:iterations)=Y(ceil(a/2),ceil(b/2),node,:);

end
if complex
  resp=respComplex;
else
  resp=respSimple;
end

axes('Position',[0.3,0.3,0.4,0.4],'FontSize',18)
plot(resp(1,:)','b-','LineWidth',3)
hold on
plot(resp(2,:)','r-','LineWidth',5)
plot([switchTime,switchTime],[-respmax,respmax*2],'k:')
tickpoints=[switchTime-preshow,switchTime,switchTime+preshow,switchTime+2*preshow,iterations];
if linear==0 & noLGNsat==0
  set(gca,'color', 'none','XTick',tickpoints,'XTickLabel',tickpoints-switchTime);
  ylabel('Response');
  xlabel('Time (iterations)');
else
  set(gca,'XTickLabel',[],'YTickLabel',[])
end
axis([switchTime-preshow,iterations,respmax*-0.05,respmax*1.05])
cmap=colormap('gray');

diff=resp(1,:)-resp(2,:);
if diff(iterations)<0, diff=-diff; end
ref=max(diff)*0.05;
sig=find(diff>=ref);
sig=sig(find(sig>=switchTime));
sig(1)-switchTime-1+(ref-diff(sig(1)-1))./(diff(sig(1))-diff(sig(1)-1))



figure(4),clf
for test=1:2
  phase=0;
  for t=1:switchTime
	I=image_cross_orientation_sine_circular(patch_diam,...
											0.5*(image_size-patch_diam),...
											grating_wavel,grating_wavel,...
											0,90,phase,phase,contrast, ...
											contrast);
	if t==1 & linear==0 & noLGNsat==0 
	  axes('Position',[0.15,0.31,0.2,0.2]),imagesc(I',[0,1]);
	  axis('equal','tight','off')
	end
	[Ion(:,:,t),Ioff(:,:,t)]=preprocess_image(I,noLGNsat);
	phase=phase+drift_rate;
  end
  for t=switchTime+1:iterations
	if test==1,
	  I=image_cross_orientation_sine_circular(patch_diam,...
											  0.5*(image_size-patch_diam),...
											  grating_wavel,grating_wavel,...
											  0,90,phase,phase,contrast, ...
											  contrast);
	  if t==iterations & linear==0 & noLGNsat==0
		axes('Position',[0.65,0.325,0.2,0.2]),imagesc(I',[0,1]);
		axis('equal','tight','off')
	  end
	else
	  I=image_circular_grating(patch_diam,0.5*(image_size-patch_diam),...
							   grating_wavel,0,phase,contrast);
	  if t==iterations & linear==0 & noLGNsat==0
		axes('Position',[0.65,0.54,0.2,0.2]),imagesc(I',[0,1]);
		axis('equal','tight','off')
	  end
	end
	drawnow;

	[Ion(:,:,t),Ioff(:,:,t)]=preprocess_image(I,noLGNsat);
	phase=phase+drift_rate;
  end
  [a,b]=size(I);
  
  %perform competition 
  if linear
	[y,Y]=linear_on_and_off(wFFon,wFFoff,Ion,Ioff,iterations);
  else
	[y,ron,roff,eon,eoff,Y]=dim_conv_on_and_off(wFFon,wFFoff,Ion,Ioff,iterations);
  end

  %record neural response
  respComplex(test,1:iterations)=calc_resp_complex(Y,ceil(a/2),ceil(b/2),node+[0:8:31]);
  
  respSimple(test,1:iterations)=Y(ceil(a/2),ceil(b/2),node,:);

end
if complex
  resp=respComplex;
else
  resp=respSimple;
end

axes('Position',[0.3,0.3,0.4,0.4],'FontSize',18)
plot(resp(1,:)','b-','LineWidth',3)
hold on
plot(resp(2,:)','r-','LineWidth',5)
plot([switchTime,switchTime],[-respmax,respmax*2],'k:')
tickpoints=[switchTime-preshow,switchTime:preshow:iterations];
if linear==0 & noLGNsat==0
  set(gca,'color', 'none','XTick',tickpoints,'XTickLabel',tickpoints-switchTime);
  ylabel('Response');
  xlabel('Time (iterations)');
else
  set(gca,'XTickLabel',[],'YTickLabel',[])
end
axis([switchTime-preshow,iterations,respmax*-0.05,respmax*1.05])
cmap=colormap('gray');

diff=resp(1,:)-resp(2,:);
if diff(iterations)<0, diff=-diff; end
ref=max(diff)*0.05;
sig=find(diff>=ref);
sig=sig(find(sig>=switchTime));
sig(1)-switchTime-1+(ref-diff(sig(1)-1))./(diff(sig(1))-diff(sig(1)-1))


